#include <R.h>
#include <Rmath.h>


void vsumc (double *arr,int *n, double *sum) {
	int i;
	double val =0;
	for(i=0;i<=*n;i++){
		val=val+ arr[i];
	}
	*sum=val/ *n;
}

/*
Here vsumc is the the looping function that will be called form 
the R script. It calculates the average of a vector.
vsumc has 3 arguments the array the vector to be averaged, the length
of th vector and the varibale that will store the average.
*/
