# BayesBD test program

To run the following code :

* Type this in **GNOME terminal**	

`>	R CMD SHLIB func.c`

*	Then run R and run the R script using

`>	source("test.R")`
 
this will load the R function into memory. Then one can call the avg() 
function from R console. One implementation of the function is shown 
in *final.R*. 
